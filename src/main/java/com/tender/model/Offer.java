package com.tender.model;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name="OFFER")
public class Offer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Integer id;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="CONTRACTOR_ID",referencedColumnName = "ID")
    private Contractor contractor;

    @Column(name="NAME")
    private String name;

    @Column(name="DESCRIPTION")
    private String description;

    @Column(name="SUBJECT_OF_PROCUREMENT")
    private String subject_of_procurement;

    @Column(name="SUM_OF_PROCUREMENT")
    private Integer sum_of_procurement;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="REQUEST_ID",referencedColumnName = "ID")
    private Request requet;
}
