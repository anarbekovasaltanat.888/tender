package com.tender.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name="CLIENT")
public class Client {
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name="BRANCH")
    private String branch;

    @Column(name="ADRESS")
    private String adress;

    @Column(name="TYPE_OF_USER")
    private String typeOfUser;

    @Column(name="BIN")
    private Integer bin;

    @Column(name="CONTACTS")
    private Integer contacts;

}
