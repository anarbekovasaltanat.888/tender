package com.tender.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Getter
@Setter
@Entity
@Table(name = "ANALYSIS")
public class Analysis {
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "YEARS_COUNT")
    private Integer yearsCount;

    @Column(name = "SUM")
    private Double sum;

    @Column(name = "coefficient")
    private Double coefficient;

    @Column(name = "FUNCTIONALITIES_COUNT")
    private Integer functionalitiesCount;

    @Column(name = "CASHFLOW_1")
    private Double cashflow1;

    @Column(name = "CASHFLOW_2")
    private Double cashflow2;

    @Column(name = "CASHFLOW_3")
    private Double cashflow3;

    @Column(name = "CASHFLOW_4")
    private Double cashflow4;

    @Column(name = "CASHFLOW_5")
    private Double cashflow5;

    @Column(name = "CASHFLOW_6")
    private Double cashflow6;

    @Column(name = "CASHFLOW_7")
    private Double cashflow7;

    @Column(name = "NPV")
    private String npv;

    @Column(name = "AAR")
    private String aar;

    @Column(name = "IRR")
    private String irr;

    @Column(name = "PI")
    private String pi;

    @Column(name = "DPP")
    private double dpp;

    @Column(name = "OCF")
    private double ocf;

    @Column(name = "DATE")
    private LocalDateTime date;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "REQUEST_ID", referencedColumnName = "ID")
    @NotFound(action = NotFoundAction.IGNORE)
    private Request request;

}
