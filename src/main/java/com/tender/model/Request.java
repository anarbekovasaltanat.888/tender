package com.tender.model;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name="REQUEST")
public class Request {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Integer id;

    @Column(name="NAME")
    private String name;

    @Column(name="DESCRIPTION")
    private String description;

    @Column(name="TYPE_OF_REQUEST")
    private String type_of_request;

    @Column(name="SUBJECT_OF_PROCUREMENT")
    private String subject_of_procurement;

    @Column(name="SUM_OF_PROCUREMENT")
    private Integer sum_of_procurement;

    @Column(name="STATUS")
    private String status;

    @Column(name = "PRICE")
    private Integer price;

    @Column(name = "RESULT")
    private String result;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="CLIENT_ID",referencedColumnName = "ID")
    private Client client;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="ORGANIZER_ID",referencedColumnName = "ID")
    private Organizer organizer;

}
