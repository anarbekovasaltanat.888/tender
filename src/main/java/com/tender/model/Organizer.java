package com.tender.model;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name="ORGANIZER")

public class Organizer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Integer id;

    @Column(name="BRANCH")
    private String branch;

    @Column(name="ADRESS")
    private String adress;

    @Column(name="TYPE_OF_USER")
    private String type_of_user;

    @Column(name="BIN")
    private Integer bin;

    @Column(name="CONTACTS")
    private Integer contacts;
}
