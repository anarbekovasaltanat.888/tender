package com.tender.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;


public class NameDto {
    private String offerName;
    private String requestName;
    private Integer bin;


}
