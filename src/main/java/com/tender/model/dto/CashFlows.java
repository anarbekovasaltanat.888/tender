package com.tender.model.dto;


import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Data
public class CashFlows {
    private String sum;
    private String cashflow1;
    private String cashflow2;
    private String cashflow3;
    private String cashflow4;
    private String cashflow5;
    private String cashflow6;
    private String cashflow7;
    private String functionalitiesCount;
    private String npv;
    private String aar;
    private String irr;
    private String dpp;
    private String pi;
    private String ocf;
}
