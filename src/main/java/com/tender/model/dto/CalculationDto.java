package com.tender.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter
@Setter
public class CalculationDto {
    @JsonProperty(value = "aar")
    private String aar;
    @JsonProperty(value = "npv")
    private String npv;
    @JsonProperty(value = "pi")
    private String pi;
    @JsonProperty(value = "irr")
    private String irr;
    @JsonProperty(value = "dpp")
    private double dpp;
    @JsonProperty(value = "ocf")
    private double ocf;
}
