package com.tender.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter
@Setter
public class CalcRequestDto {
    @JsonProperty(value = "years")
    private int years;
    @JsonProperty(value = "cashFlows")
    private double[] cashFlows;
    @JsonProperty(value = "discountRate")
    private double discountRate;
}
