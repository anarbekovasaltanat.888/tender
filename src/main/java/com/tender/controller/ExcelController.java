package com.tender.controller;

import com.tender.model.dto.CalcRequestDto;
import com.tender.service.impl.ExcelGenerator;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

@RestController
@RequestMapping("/excel")
public class ExcelController {
    private final ExcelGenerator generator;

    public ExcelController(ExcelGenerator generator) {
        this.generator = generator;
    }


    @PostMapping("/export-to-excel")
    public void exportIntoExcelFile(HttpServletResponse response, @RequestBody CalcRequestDto calcRequestDto) throws IOException {
        response.setContentType("application/octet-stream");
        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=irr.xlsx";
        response.setHeader(headerKey, headerValue);

        ExcelGenerator generator = new ExcelGenerator();
        generator.generateExcelFile(response, calcRequestDto.getCashFlows(), calcRequestDto.getDiscountRate(), calcRequestDto.getYears());
    }
}
