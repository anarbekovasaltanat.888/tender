package com.tender.controller;

import com.tender.model.Organizer;
import com.tender.service.OrganizerService;
//import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

//@Api(value = "Swagger2DemoRestController", description = "REST APIs related to item Entity!")
@RestController
@RequestMapping("/organizers")
public class OrganizerController {

    private final OrganizerService organizerService;

    @Autowired
    public OrganizerController(OrganizerService organizerService) {
        this.organizerService = organizerService;
    }

    @GetMapping("/")
    public List<Organizer> findAll() {

        return organizerService.findAll();
    }

    @GetMapping("/{id}")
    public Organizer findAllById(@PathVariable("id") Integer id) {

        return organizerService.findAllById(id);
    }

    @DeleteMapping("/{id}")
    private void deleteById(@PathVariable("id") Integer id) {

        organizerService.deleteById(id);
    }

    //creating post mapping that post the book detail in the database
    @PostMapping("/")
    private Organizer postById(@RequestBody Organizer organizer) {

        return organizerService.postById(organizer);
    }

    //creating put mapping that updates the book detail
    @PutMapping("/{id}")
    private Organizer updateById(@PathVariable("id") Integer id,@RequestBody Organizer organizer) {

        return organizerService.updateById(id,organizer);
    }
}
