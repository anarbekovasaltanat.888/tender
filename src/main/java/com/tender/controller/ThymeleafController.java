package com.tender.controller;

import com.tender.model.Analysis;
import com.tender.model.Client;
import com.tender.model.Organizer;
import com.tender.model.Request;
import com.tender.model.dto.CalcRequestDto;
import com.tender.model.dto.CalculationDto;
import com.tender.model.dto.CashFlows;
import com.tender.service.AnalysisService;
import com.tender.service.ClientService;
import com.tender.service.CoefficientService;
import com.tender.service.ContractorService;
import com.tender.service.OfferService;
import com.tender.service.OrganizerService;
import com.tender.service.RequestService;
import com.tender.service.ResultService;
import com.tender.service.impl.CalculationImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.IOException;
import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Controller
public class ThymeleafController {
    private final ClientService clientService;
    private final ContractorService contractorService;
    private final OfferService offerService;
    private final OrganizerService organizerService;
    private final RequestService requestService;
    private final AnalysisService analysisService;
    private final CalculationImpl calculation;
    private final CoefficientService coefficientService;
    private final ResultService resultService;

    @Autowired
    public ThymeleafController(ClientService clientService,
                               ContractorService contractorService,
                               OfferService offerService,
                               OrganizerService organizerService,
                               RequestService requestService, AnalysisService analysisService, CalculationImpl calculation, CoefficientService coefficientService, ResultService resultService) {
        this.clientService = clientService;
        this.contractorService = contractorService;
        this.offerService = offerService;
        this.organizerService = organizerService;
        this.requestService = requestService;
        this.analysisService = analysisService;
        this.calculation = calculation;
        this.coefficientService = coefficientService;
        this.resultService = resultService;
    }

    @GetMapping(value = "/")
    public String home(Model model) {
        model.addAttribute("clientList", clientService.findAll());
        return "index";
    }

    @GetMapping(value = "/admin")
    public String admin(Model model) {
        model.addAttribute("lotsList", requestService.sortedById());
        return "admin";
    }

    @GetMapping(value = "/edit")
    public String edit(@RequestParam("id")Integer id, Model model) {
        model.addAttribute("id", id);
        return "edit";
    }


    @GetMapping(value = "/lots")
    public String lots(Model model) {
        model.addAttribute("lotsList", requestService.sortedById());
        return "lots";
    }

    @GetMapping(value = "/calc")
    public String calc(@RequestParam(value = "reqId",required = false) Integer reqId,
                       Model model) {
        if (reqId!=null)
            model.addAttribute("reqId", reqId);
        else
            model.addAttribute("reqId", 4);
        return "calc";
    }

    @PostMapping(value = "/accept")
    public String accept(@RequestParam("reqId") Integer id){
        Request req = requestService.findAllById(id);
        req.setStatus("Опубликовано");
        requestService.updateById(id, req);
        return "redirect:/lots";
    }

    @PostMapping(value = "/decline")
    public String decline(@RequestParam("reqId") Integer id){
        Request req = requestService.findAllById(id);
        req.setStatus("Отклонено");
        requestService.updateById(id, req);
        return "redirect:/lots";
    }

    @GetMapping(value = "/result")
    public String result(@RequestParam(value = "id",required = false) Integer id,
                         Model model) {
        List<CashFlows> cashFlows = resultService.calculateResult();
        model.addAttribute("fiveList", cashFlows);
        boolean irr = false;
        boolean npv = false;
        boolean aar = false;
        boolean dpp = false;
        boolean pi = false;
        int res = 0;
        Analysis analysis = new Analysis();
        if (id!=null) {
            analysis = analysisService.findAllById(id);
            if(Double.valueOf(analysis.getAar().replace(",", ".")) > 1) {
                ++res;
                aar = true;
            }
            if(Double.valueOf(analysis.getNpv().replace(",", ".")) > 0) {
                ++res;
                npv = true;
            }
            if(Double.valueOf(analysis.getIrr().replace(",", ".")) > analysis.getCoefficient()) {
                ++res;
                irr = true;
            }
            if(Double.valueOf(analysis.getPi().replace(",", ".")) > 1) {
                ++res;
                pi = true;
            }
            if(analysis.getDpp() <= 3) {
                ++res;
                dpp = true;
            }
            model.addAttribute("npv", npv);
            model.addAttribute("aar", aar);
            model.addAttribute("irr", irr);
            model.addAttribute("pi", pi);
            model.addAttribute("dpp", dpp);
            model.addAttribute("analysis", analysis);
        } else {
            model.addAttribute("analysis", analysisService.findF().get(0));
        }
        if (res>=3)
            model.addAttribute("analysisResult", "Потенциально привлекательно");
        else
            model.addAttribute("analysisResult", "Потенциально не привлекательно");
        model.addAttribute("reqId", analysis.getRequest().getId());
        return "result";
    }

    @GetMapping(value = "/res")
    public String res(Model model) {
        List<CashFlows> cashFlows = resultService.calculateResult();
        model.addAttribute("fiveList", cashFlows);
        return "res";
    }

    @GetMapping(value = "/addlot")
    public String addLot(Model model) {
        return "add";
    }

    @PostMapping(value = "/addlot")
    public String addLot(@RequestParam(name = "name", defaultValue = "") String name,
                         @RequestParam(name = "description", defaultValue = "") String description,
                         @RequestParam(name = "price", defaultValue = "0") Integer price,
                         @RequestParam(name = "sum", defaultValue = "0") Integer sum,
                         @RequestParam(name = "subject", defaultValue = "") String subject,
                         @RequestParam(name = "type", defaultValue = "") String type) {
        Request req = new Request();
        Client cl = clientService.findAllById(1);
        Organizer org = organizerService.findAllById(1);
        req.setName(name);
        req.setDescription(description);
        req.setType_of_request(type);
        req.setSubject_of_procurement(subject);
        req.setSum_of_procurement(sum);
        req.setPrice(price);
        req.setStatus("Новая");
        req.setClient(cl);
        req.setOrganizer(org);
        req.setResult("Не проверено");

        requestService.postById(req);

        return "redirect:/lots";
    }

    @PostMapping(value = "/addAnalysis")
    public String addAnalysis(@RequestParam(name = "sum", defaultValue = "0") Double sum,
                              @RequestParam(name = "coefficient", defaultValue = "0") Double coefficient,
                              @RequestParam(name = "funcCount", defaultValue = "0") Integer funcCount,
                              @RequestParam(name = "yearsCount", defaultValue = "00") Integer yearsCount,
                              @RequestParam(name = "cashflow1", defaultValue = "0") Double cashflow1,
                              @RequestParam(name = "cashflow2", defaultValue = "0") Double cashflow2,
                              @RequestParam(name = "cashflow3", defaultValue = "0") Double cashflow3,
                              @RequestParam(name = "cashflow4", defaultValue = "0") Double cashflow4,
                              @RequestParam(name = "cashflow5", defaultValue = "0") Double cashflow5,
                              @RequestParam(name = "cashflow6", defaultValue = "0") Double cashflow6,
                              @RequestParam(name = "cashflow7", defaultValue = "0") Double cashflow7,
                              @RequestParam(name = "reqId", required = false) Integer reqId,
                              Model model) throws IOException {
        CalcRequestDto calcRequestDto = new CalcRequestDto();
        double[] cashflows = { -sum, cashflow1, cashflow2, cashflow3, cashflow4, cashflow5, cashflow6, cashflow7};
        calcRequestDto.setCashFlows(cashflows);
        calcRequestDto.setYears(yearsCount);
        calcRequestDto.setDiscountRate(coefficient);
        CalculationDto calculationDto = calculation.calculate(cashflows,coefficient,yearsCount);
        Double funcCoeficient = coefficientService.calculateCoefficient(sum, funcCount);
        int res = 0;
        if(Double.valueOf(calculationDto.getAar().replace(",", ".")) > 1)
            ++res;
        if(Double.valueOf(calculationDto.getNpv().replace(",", ".")) > 0)
            ++res;
        if(Double.valueOf(calculationDto.getIrr().replace(",", ".")) > coefficient)
            ++res;
        if(Double.valueOf(calculationDto.getPi().replace(",", ".")) > 1)
            ++res;
        if(calculationDto.getDpp() < 3)
            ++res;
        Request req = requestService.findAllById(reqId);
        if(res >= 3)
            req.setResult("Потенциально привлекательно");
        else
            req.setResult("Потенциально не привлекательно");
        Analysis analysis = new Analysis();
        if (reqId!=null) {
            if (analysisService.findByRequestId(reqId) != null)
                analysis = analysisService.findByRequestId(reqId);
        }
        analysis.setSum(sum);
        analysis.setCashflow1(cashflow1);
        analysis.setCashflow2(cashflow2);
        analysis.setCashflow3(cashflow3);
        analysis.setCashflow4(cashflow4);
        analysis.setCashflow5(cashflow5);
        analysis.setCashflow6(cashflow6);
        analysis.setCashflow7(cashflow7);
        analysis.setCoefficient(coefficient);
        analysis.setYearsCount(yearsCount);
        analysis.setFunctionalitiesCount(funcCount);
        analysis.setNpv(calculationDto.getNpv());
        analysis.setAar(calculationDto.getAar());
        analysis.setIrr(calculationDto.getIrr());
        analysis.setPi(calculationDto.getPi());
        analysis.setDpp(calculationDto.getDpp());
        analysis.setOcf(funcCoeficient);
        analysis.setDate(LocalDateTime.now());
        analysis.setRequest(requestService.findAllById(reqId));
        analysisService.postById(analysis);
        model.addAttribute("reqId",analysis.getRequest().getId());

        return "redirect:/result?id=" + analysis.getId();
    }

    @GetMapping(value = "/filter")
    public String findByFilter(@RequestParam(value = "id", required = false) Integer id,
                               @RequestParam(value = "name",required = false) String name,
                               @RequestParam(value = "subject",required = false) String subject,
                               @RequestParam(value = "type",required = false) String type,
                               @RequestParam(value = "status",required = false) String status,
                               @RequestParam(value = "priceFrom",required = false) Integer priceFrom,
                               @RequestParam(value = "priceTo",required = false) Integer priceTo,
                               @RequestParam(value = "result", required = false) String result ,
                               Model model){
        List<Request> requestList = requestService.findByFilter(id, name, type, subject,
                status, priceFrom, priceTo, result);
        model.addAttribute("lotsList", requestList);
        return "lots";
    }

    @GetMapping(value = "/deleteById/{id}")
    public String deleteById(@PathVariable("id") Integer id){
        Integer analysisId = analysisService.findByRequestId(id).getId();
        analysisService.deleteById(analysisId);
        requestService.deleteById(id);
        return "redirect:/admin";
    }

    @PostMapping(value = "/editLot/{id}")
    public String editLot(@PathVariable("id") Integer id,
                         @RequestParam(name = "name", required = false) String name,
                         @RequestParam(name = "description", required = false) String description,
                         @RequestParam(name = "price",required = false) Integer price,
                         @RequestParam(name = "sum",required = false) Integer sum,
                         @RequestParam(name = "subject", required = false) String subject,
                         @RequestParam(name = "type", required = false) String type,
                          Model model) {
        Request req = requestService.findAllById(id);
        if(name!=null && name!="")
            req.setName(name);
        if(description!=null && description!="")
            req.setDescription(description);
        if(type!=null && description!="")
            req.setType_of_request(type);
        if(subject!=null && subject!="")
            req.setSubject_of_procurement(subject);
        if(sum!=null)
            req.setSum_of_procurement(sum);
        if(price!=null)
            req.setPrice(price);
        req.setStatus("In progress");
        requestService.updateById(id, req);

        return "redirect:/admin";
    }

}
