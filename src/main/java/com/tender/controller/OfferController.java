package com.tender.controller;


import com.tender.model.Offer;
import com.tender.model.dto.NameDto;
import com.tender.service.OfferService;
//import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

//@Api(value = "Swagger2DemoRestController", description = "REST APIs related to item Entity!")
@RestController
@RequestMapping("/offers")
public class OfferController {
    private final OfferService offerService;

    @Autowired
    public OfferController(OfferService offerService) {
        this.offerService = offerService;
    }

    @GetMapping("/")
    public List<Offer> findAll() {

        return offerService.findAll();
    }

    @GetMapping("/{id}")
    public Offer findAllById(@PathVariable("id") Integer id) {

        return offerService.findAllById(id);
    }

    @DeleteMapping("/{id}")
    private void deleteById(@PathVariable("id") Integer id) {

        offerService.deleteById(id);
    }

    //creating post mapping that post the book detail in the database
    @PostMapping("/")
    private Offer postById(@RequestBody Offer offer) {

        return offerService.postById(offer);
    }

    //creating put mapping that updates the book detail
    @PutMapping("/{id}")
    private Offer updateById(@PathVariable("id") Integer id,@RequestBody Offer offer) {

        return offerService.updateById(id,offer);
    }

//    @GetMapping("/getName/{id}")
//    private NameDto findName(@PathVariable("id") Integer id){
//        return offerService.findName(id);
//    }
}
