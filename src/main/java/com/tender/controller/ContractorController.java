package com.tender.controller;

import com.tender.model.Contractor;
import com.tender.service.ContractorService;
//import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

//@Api(value = "Swagger2DemoRestController", description = "REST APIs related to item Entity!")
@RestController
@RequestMapping("/contractors")
public class ContractorController {
    private final ContractorService contractorService;

    @Autowired
    public ContractorController(ContractorService contractorService) {

        this.contractorService = contractorService;
    }

    @GetMapping("/")
    public List<Contractor> findAll() {

        return contractorService.findAll();
    }

    @GetMapping("/{id}")
    public Contractor findAllById(@PathVariable("id") Integer id) {

        return contractorService.findAllById(id);
    }

    @DeleteMapping("/{id}")
    private void deleteById(@PathVariable("id") Integer id) {

        contractorService.deleteById(id);
    }

    //creating post mapping that post the book detail in the database
    @PostMapping("/")
    private Contractor postById(@RequestBody Contractor contractor) {

        return contractorService.postById(contractor);
    }

    //creating put mapping that updates the book detail
    @PutMapping("/{id}")
    private Contractor updateById(@PathVariable("id") Integer id, @RequestBody Contractor contractor) {
        return contractorService.updateById(id, contractor);
    }
}
