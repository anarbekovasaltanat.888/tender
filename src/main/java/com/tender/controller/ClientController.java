package com.tender.controller;

import com.tender.model.Client;
import com.tender.model.Contractor;
import com.tender.service.ClientService;
//import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

//@Api(value = "Swagger2DemoRestController", description = "REST APIs related to item Entity!")
@RestController
@RequestMapping("/clients")
public class ClientController {
    private final ClientService clientService;

    @Autowired
    public ClientController(ClientService clientService) {
        this.clientService = clientService;
    }

    @GetMapping("/")
    public List<Client> findAll() {
        return clientService.findAll();
    }
    @GetMapping("/{id}")
    public Client findAllById(@PathVariable("id") Integer id) {

        return clientService.findAllById(id);
    }

    @DeleteMapping("/{id}")
    private void deleteById(@PathVariable("id") Integer id) {

         clientService.deleteById(id);
    }

    //creating post mapping that post the book detail in the database
    @PostMapping("/")
    private Client postById(@RequestBody Client client) {

        return clientService.postById(client);
    }

    //creating put mapping that updates the book detail
    @PutMapping("/{id}")
    private Client updateById(@PathVariable("id") Integer id, @RequestBody Client client) {
        return clientService.updateById(id, client);
    }
}
