package com.tender.controller;

import com.tender.model.Request;
import com.tender.service.RequestService;
//import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

//@Api(value = "Swagger2DemoRestController", description = "REST APIs related to item Entity!")
@RestController
@RequestMapping("/requests")
public class RequestController {
    private final RequestService requestService;

    @Autowired
    public RequestController(RequestService requestService) {

        this.requestService = requestService;
    }

    @GetMapping("/")
    public List<Request> findAll() {
        return requestService.findAll();
    }

    @GetMapping("/{id}")
    public Request findAllById(@PathVariable("id") Integer id) {
        return requestService.findAllById(id);
    }

    @DeleteMapping("/{id}")
    private void deleteById(@PathVariable("id") Integer id) {

        requestService.deleteById(id);
    }

    //creating post mapping that post the book detail in the database
    @PostMapping("/")
    private Request postById(@RequestBody Request request) {
        return requestService.postById(request);
    }

    //creating put mapping that updates the book detail
    @PutMapping("/{id}")
    private Request updateById(@PathVariable("id") Integer id,@RequestBody Request request) {
        return requestService.updateById(id,request);
    }

}
