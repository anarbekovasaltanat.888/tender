package com.tender.controller;

import com.tender.model.dto.CalcRequestDto;
import com.tender.model.dto.CalculationDto;
import com.tender.service.impl.CalculationImpl;
import com.tender.service.impl.ExcelGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
@RequestMapping("/calculations")
public class CalculationController {

    private final CalculationImpl calculationImpl;
    private final ExcelGenerator excelGenerator;

    @Autowired
    public CalculationController(CalculationImpl calculationImpl, ExcelGenerator excelGenerator) {
        this.calculationImpl = calculationImpl;
        this.excelGenerator = excelGenerator;
    }


    @PostMapping("/calculate")
    public CalculationDto calculate(@RequestBody CalcRequestDto calcRequestDto) throws IOException {
        return calculationImpl.calculate(calcRequestDto.getCashFlows(),
                calcRequestDto.getDiscountRate(),
                calcRequestDto.getYears());
    }
}
