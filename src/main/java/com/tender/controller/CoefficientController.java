package com.tender.controller;

import com.tender.service.CoefficientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/coefficient")
public class CoefficientController {
    private final CoefficientService coefficientService;

    @Autowired
    public CoefficientController(CoefficientService coefficientService) {
        this.coefficientService = coefficientService;
    }

    @GetMapping("/calculate")
    public Double calculateCoeficient(@RequestParam("sum") Double sum,
                                      @RequestParam("quantity") int quantity){
        return coefficientService.calculateCoefficient(sum, quantity);
    }
}
