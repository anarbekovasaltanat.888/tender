package com.tender.repository;

import com.tender.model.Request;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface RequestRepository extends JpaRepository<Request,Integer> {
    @Query(value = "select r from Request r " +
            "where r.id = :id ")
    List<Request> findAllById(@Param("id") Integer id);

    @Query(value = "select * from request r order by r.id desc ", nativeQuery = true)
    List<Request> findAllSortedById();

    @Query(value = "select * from Request r " +
            "where (r.id = :id or :id is null or :id = 0) " +
            "and (r.name like '%' || :name || '%' or :name is null or :name = '') " +
            "and (r.type_of_request like '%' || :type || '%' or :type is null or :type = '') " +
            "and (r.subject_of_procurement like '%' || :subject || '%' or :subject is null or :subject = '') " +
            "and (r.status like '%' || :status || '%' or :status is null or :status = '') " +
            "and (r.price >= :priceFrom or :priceFrom is null or :priceFrom = 0) " +
            "and (r.price <= :priceTo or :priceTo is null or :priceTo = 0) " +
            "and (r.result like '%' || :result || '%' or :result is null or :result = '') ", nativeQuery = true)
    List<Request> findAllByFilter(@Param("id") Integer id,
                                  @Param("name") String name,
                                  @Param("type") String type,
                                  @Param("subject") String subject,
                                  @Param("status") String status,
                                  @Param("priceFrom") Integer priceFrom,
                                  @Param("priceTo") Integer priceTo,
                                  @Param("result") String result);

}
