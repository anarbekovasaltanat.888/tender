package com.tender.repository;

import com.tender.model.Contractor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ContractorRepository extends JpaRepository<Contractor, Integer> {

//    List<Contractor> findAllById(@Param("id") Integer id);

//    @Query(value = "select c from Contractor c " +
//            "where c.id = :id ")
//    List<Contractor> findAllById(@Param("id") Integer id);

}
