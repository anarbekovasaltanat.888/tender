package com.tender.repository;

import com.tender.model.Organizer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface OrganizerRepository extends JpaRepository<Organizer,Integer> {
    @Query(value = "select o from Organizer o " +
            "where o.id = :id ")
    List<Organizer> findAllById(@Param("id") Integer id);
}
