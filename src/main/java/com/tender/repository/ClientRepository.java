package com.tender.repository;

import com.tender.model.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ClientRepository extends JpaRepository<Client, Integer> {
//
//    @Query(value = "select c from Client c " +
//            "where c.id = :id ")
//    List<Client> findAllById(@Param("id") Integer id);


}
