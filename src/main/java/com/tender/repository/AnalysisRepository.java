package com.tender.repository;

import com.tender.model.Analysis;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface AnalysisRepository extends JpaRepository<Analysis, Integer> {

    @Query(value = "select * from analysis a order by a.date desc limit 5 ", nativeQuery = true)
    List<Analysis> findFirstFive();


    @Query(value = "select * from analysis a " +
            "where a.request_id = :req_id " +
            "order by a.id desc " +
            "limit 1 ", nativeQuery = true)
    Analysis findByRequestId(@Param("req_id")Integer id);

}
