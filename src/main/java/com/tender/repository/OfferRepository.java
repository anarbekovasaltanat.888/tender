package com.tender.repository;

import com.tender.model.Offer;
import com.tender.model.dto.NameDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface OfferRepository extends JpaRepository<Offer,Integer> {
    @Query(value = "select o from Offer o " +
            "where o.id = :id ")
    List<Offer> findAllById(@Param("id") Integer id);

//    @Query(value = "select o.name as offerName , r.name as requestName ,c.bin as bin from Offer o " +
//            "join Request  r on o.requet.id=r.id " +
//            "join Contractor c on o.contractor.id=c.id " +
//            "where o.id = :id ")
//    NameDto findName(@Param("id") Integer id);

}
