package com.tender.service;

import com.tender.model.Client;

import java.util.List;

public interface ClientService {
    List<Client> findAll();
    Client findAllById(Integer id);

    void deleteById(Integer id);

    Client postById(Client client);

    Client updateById(Integer id,Client client);

    void save(Client client);

}
