package com.tender.service.impl;

import com.tender.model.dto.CalculationDto;
import org.apache.catalina.connector.Response;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.DecimalFormat;

@Service
public class CalculationImpl {

    private final ExcelGenerator excelGenerator;
    private final DecimalFormat df = new DecimalFormat("0.00");

    public CalculationImpl(ExcelGenerator excelGenerator) {
        this.excelGenerator = excelGenerator;
    }

    public CalculationDto calculate(double[] cashFlows, double discountRate, int years) throws IOException {
        HttpServletResponse response = new Response();
        CalculationDto calculationDto = new CalculationDto();
        calculationDto.setNpv(df.format(calculateNPV(cashFlows, discountRate)));
        calculationDto.setAar(df.format(calculateAAR(cashFlows)));
        calculationDto.setPi(df.format(calculatePI(cashFlows, discountRate)));
        calculationDto.setDpp(calculatePaybackPeriod(cashFlows));
        excelGenerator.generateExcelFile(response,cashFlows,discountRate,years);
        calculationDto.setIrr(df.format(excelGenerator.getIRR()));
        System.out.println("IRR: + " + excelGenerator.getIRR());
        return calculationDto;
    }

    private double calculateNPV(double[] cashFlows, double discountRate) {
        double npv = 0.0;
        for (int i = 1; i < cashFlows.length; i++) {
            npv += cashFlows[i] / Math.pow(1 + discountRate, i);
        }
        return npv + cashFlows[0];
    }

    private double calculateAAR(double[] cashFlows) {
        double aar = 0.0;
        for (int i = 1; i < cashFlows.length; i++) {
            aar += cashFlows[i];
        }
        return aar / (-cashFlows[0]);
    }

    private double calculatePI(double[] cashFlows, double discountRate) {
        double npv = calculateNPV(cashFlows, discountRate);
        double initialInvestment = cashFlows[0];
        return 1+(-npv / initialInvestment);
    }

    private int calculatePaybackPeriod(double[] cashFlows) {
        double cumulativeCashFlow = 0.0;
        int paybackPeriod = 0;
        for (int i = 0; i < cashFlows.length; i++) {
            cumulativeCashFlow += cashFlows[i];
            if (cumulativeCashFlow >= 0) {
                paybackPeriod = i + 1;
                break;
            }
        }
        return paybackPeriod;
    }

}
