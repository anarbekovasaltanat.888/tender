package com.tender.service.impl;

import com.tender.model.Organizer;
import com.tender.repository.OrganizerRepository;
import com.tender.service.OrganizerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class OrganizerServiceImpl implements OrganizerService {

    private final OrganizerRepository organizerRepository;
    @Autowired
    public OrganizerServiceImpl(OrganizerRepository organizerRepository){
        this.organizerRepository=organizerRepository;
    }

    @Override
    public List<Organizer> findAll(){
        return organizerRepository.findAll();}


    @Override
    public  Organizer findAllById(Integer id){

        return  organizerRepository.findById(id).orElse(null);
    }

    @Override
    public  void deleteById(Integer id){
        organizerRepository.deleteById(id);
    }

    @Override
    public  Organizer postById(Organizer organizer){

        return  organizerRepository.save(organizer);
    }

    @Override
    public  Organizer updateById(Integer id,Organizer organizer){
        Organizer organizerNew=findAllById(id);
        organizerNew.setAdress(organizer.getAdress());
        organizerNew.setBranch(organizer.getBranch());
        organizerNew.setBin(organizer.getBin());
        organizerNew.setContacts(organizer.getContacts());
        organizerNew.setType_of_user(organizer.getType_of_user());
        organizerRepository.save(organizerNew);
        return organizerNew;
    }

}
