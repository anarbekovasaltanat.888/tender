package com.tender.service.impl;

import com.tender.model.Analysis;
import com.tender.model.dto.CashFlows;
import com.tender.service.AnalysisService;
import com.tender.service.ResultService;
import org.springframework.stereotype.Service;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

@Service
public class ResultServiceImpl implements ResultService {
    private final AnalysisService analysisService;

    public ResultServiceImpl(AnalysisService analysisService) {
        this.analysisService = analysisService;
    }

    public List<CashFlows> calculateResult(){
        DecimalFormat decimalFormat = new DecimalFormat("#.00");
        List<Analysis> analysisList = analysisService.findF();
        List<CashFlows> cashFlows = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            CashFlows cashFlow = new CashFlows();
            cashFlow.setCashflow1(decimalFormat.format(analysisList.get(i).getCashflow1()));
            cashFlow.setCashflow2(decimalFormat.format(analysisList.get(i).getCashflow2()));
            cashFlow.setCashflow3(decimalFormat.format(analysisList.get(i).getCashflow3()));
            cashFlow.setCashflow4(decimalFormat.format(analysisList.get(i).getCashflow4()));
            cashFlow.setCashflow5(decimalFormat.format(analysisList.get(i).getCashflow5()));
            cashFlow.setCashflow6(decimalFormat.format(analysisList.get(i).getCashflow6()));
            cashFlow.setCashflow7(decimalFormat.format(analysisList.get(i).getCashflow7()));
            cashFlow.setSum(decimalFormat.format(analysisList.get(i).getSum()));
            cashFlow.setAar(String.valueOf(analysisList.get(i).getAar()));
            cashFlow.setIrr(String.valueOf(analysisList.get(i).getIrr()));
            cashFlow.setNpv(String.valueOf(analysisList.get(i).getNpv()));
            cashFlow.setOcf(decimalFormat.format(analysisList.get(i).getOcf()));
            cashFlow.setDpp(decimalFormat.format(analysisList.get(i).getDpp()));
            cashFlow.setPi(String.valueOf(analysisList.get(i).getPi()));
            cashFlow.setFunctionalitiesCount(String.valueOf(analysisList.get(i).getFunctionalitiesCount()));
            cashFlows.add(cashFlow);
        }
        return cashFlows;
    }
}
