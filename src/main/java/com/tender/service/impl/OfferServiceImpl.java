package com.tender.service.impl;

import com.tender.model.Client;
import com.tender.model.Offer;
import com.tender.model.dto.NameDto;
import com.tender.repository.OfferRepository;
import com.tender.service.OfferService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class OfferServiceImpl implements OfferService {

    private final OfferRepository offerRepository ;

    @Autowired
    public OfferServiceImpl (OfferRepository offerRepository){

        this.offerRepository=offerRepository;
    }

    @Override
    public List<Offer> findAll() {
        return offerRepository.findAll();}

    @Override
    public  Offer findAllById(Integer id){

        return  offerRepository.findById(id).orElse(null);
    }

    @Override
    public  void deleteById(Integer id){
        offerRepository.deleteById(id);
    }

    @Override
    public  Offer postById(Offer offer){
        return  offerRepository.save(offer);
    }

    @Override
    public  Offer updateById(Integer id,Offer offer){
        Offer offerNew = findAllById(id);
        offerNew.setName(offer.getName());
        offerNew.setDescription(offer.getDescription());
        offerNew.setSubject_of_procurement(offer.getSubject_of_procurement());
        offerNew.setSum_of_procurement(offer.getSum_of_procurement());
        offerRepository.save(offerNew);
        return offerNew;
    }

//    @Override
//    public NameDto findName(Integer id){
//        return offerRepository.findName(id);
//    }
}
