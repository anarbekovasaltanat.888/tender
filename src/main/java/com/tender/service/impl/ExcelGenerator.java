package com.tender.service.impl;
import java.io.IOException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFFormulaEvaluator;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

@Service
public class ExcelGenerator {

    private double irr;
    private XSSFWorkbook workbook;
    private XSSFSheet sheet;

    public ExcelGenerator() {
        workbook = new XSSFWorkbook();
    }

    private void writeHeader() {
        sheet = workbook.createSheet();
        Row row = sheet.createRow(0);
        CellStyle style = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        font.setBold(true);
        font.setFontHeight(16);
        style.setFont(font);
        createCell(row, 0, "Sum", style);
        createCell(row, 1, "Cashflow 1", style);
        createCell(row, 2, "Cashflow 2", style);
        createCell(row, 3, "Cashflow 3", style);
        createCell(row, 3, "Cashflow 4", style);
        createCell(row, 3, "Cashflow 5", style);
    }

    private void createCell(Row row, int columnCount, Object valueOfCell, CellStyle style) {
        sheet.autoSizeColumn(columnCount);
        Cell cell = row.createCell(columnCount);
        if (valueOfCell instanceof Integer) {
            cell.setCellValue((Integer) valueOfCell);
        } else if (valueOfCell instanceof Long) {
            cell.setCellValue((Long) valueOfCell);
        } else if (valueOfCell instanceof String) {
            cell.setCellValue((String) valueOfCell);
        } else if (valueOfCell instanceof Double) {
            cell.setCellValue((Double   ) valueOfCell);
        } else {
            cell.setCellValue((Boolean) valueOfCell);
        }
        cell.setCellStyle(style);
    }

    private void write(double[] cashFlows, double discountRate, int years) {
        int rowCount = 1;
        CellStyle style = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        font.setFontHeight(14);
        style.setFont(font);

        Row row = sheet.createRow(1);
        for (int i = 0; i < years; i++) {
            createCell(row, i, cashFlows[i], style);
        }
        Row row1 = sheet.createRow(3);
        char c = 'A';
        c+=years;
        Cell formualaCell = row1.createCell(3);
        formualaCell.setCellFormula(new StringBuilder().append("IRR(A2:").append(c).append("2)").toString());
        XSSFFormulaEvaluator formulaEvaluator =
                workbook.getCreationHelper().createFormulaEvaluator();
        formulaEvaluator.evaluateFormulaCell(formualaCell);
//        createCell(row1, 6,"=ВСД(A2:F2)", style);



//        for (Student record : studentList) {
//            Row row = sheet.createRow(rowCount++);
//            int columnCount = 0;
//            createCell(row, columnCount++, record.getId(), style);
//            createCell(row, columnCount++, record.getStudentName(), style);
//            createCell(row, columnCount++, record.getEmail(), style);
//            createCell(row, columnCount++, record.getMobileNo(), style);
//        }
        irr = formualaCell.getNumericCellValue();

    }

    public void generateExcelFile(HttpServletResponse response, double[] cashFlows, double discountRate, int years) throws IOException {
        writeHeader();
        write(cashFlows, discountRate, years);
//        ServletOutputStream outputStream = response.getOutputStream();
//        workbook.write(outputStream);
        workbook.close();
//        outputStream.close();
    }

    public double getIRR(){
        return irr;
    }
}