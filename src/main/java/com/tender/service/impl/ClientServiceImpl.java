package com.tender.service.impl;

import com.tender.model.Client;
import com.tender.model.Contractor;
import com.tender.model.dto.NameDto;
import com.tender.repository.ClientRepository;
import com.tender.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ClientServiceImpl implements ClientService {

    private final ClientRepository clientRepository;

    @Autowired
    public ClientServiceImpl(ClientRepository clientRepository){

        this.clientRepository = clientRepository;
    }

    @Override
    public List<Client> findAll() {
        return clientRepository.findAll();
    }

    @Override
    public  Client findAllById(Integer id){
        return  clientRepository.findById(id).orElse(null);
    }

    @Override
    public  void deleteById(Integer id){
        clientRepository.deleteById(id);
    }

    @Override
    public  Client postById(Client client){
        clientRepository.count();
        return  clientRepository.save(client);
    }

    @Override
    public  Client updateById(Integer id,Client client){
        Client clientNew = findAllById(id);
        clientNew.setAdress(client.getAdress());
        clientNew.setBranch(client.getBranch());
        clientNew.setBin(client.getBin());
        clientNew.setContacts(client.getContacts());
        clientNew.setTypeOfUser(client.getTypeOfUser());
        clientRepository.save(clientNew);
        return clientNew;
    }

    @Override
    public void save(Client client) {
        clientRepository.save(client);
    }


}
