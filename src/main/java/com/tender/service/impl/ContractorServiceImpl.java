package com.tender.service.impl;

import com.tender.model.Contractor;
import com.tender.repository.ContractorRepository;
import com.tender.service.ContractorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.List;

@Service
public class ContractorServiceImpl implements ContractorService {

    private final ContractorRepository contractorRepository;

    @Autowired
    public ContractorServiceImpl(ContractorRepository contractorRepository){
        this.contractorRepository = contractorRepository;
    }

    @Override
    public List<Contractor> findAll() {

        return contractorRepository.findAll();
    }



    @Override
    public Contractor findAllById(Integer id){

        return contractorRepository.findById(id).orElse(null);
    }

    @Override
    public  void deleteById(Integer id){
        contractorRepository.deleteById(id);
    }

    @Override
    public Contractor postById(Contractor contractor){

        return  contractorRepository.save(contractor);
    }

    @Override
    public  Contractor updateById(Integer id, Contractor contractor){
        Contractor contractorNew = findAllById(id);
        contractorNew.setAdress(contractor.getAdress());
        contractorNew.setBranch(contractor.getBranch());
        contractorNew.setBin(contractor.getBin());
        contractorNew.setContacts(contractor.getContacts());
        contractorNew.setTypeOfUser(contractor.getTypeOfUser());
        contractorRepository.save(contractorNew);
        return contractorNew;
    }


}
