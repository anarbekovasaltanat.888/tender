package com.tender.service.impl;

import com.tender.model.Request;
import com.tender.repository.RequestRepository;
import com.tender.service.RequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class RequestServiceImpl implements RequestService {

    private final RequestRepository requestRepository;

    @Autowired
    public RequestServiceImpl(RequestRepository requestRepository){
        this.requestRepository=requestRepository;
    }

    @Override
    public List<Request> findAll(){
        return requestRepository.findAll();}

    @Override
    public  Request findAllById(Integer id){
        return  requestRepository.findById(id).orElse(null);
    }

    @Override
    public  void deleteById(Integer id){
        requestRepository.deleteById(id);
    }

    @Override
    public  Request postById(Request request){
        return  requestRepository.save(request);
    }

    @Override
    public  Request updateById(Integer id,Request request){
        Request requestNew=findAllById(id);
//        if(request.getName()!=null ||
//                request.getName()!="" ||
//                !request.getName().isBlank() ||
//                !request.getName().isEmpty())
            requestNew.setName(request.getName());
//        if(request.getDescription()!=null ||
//                request.getDescription()!="" ||
//                !request.getDescription().isBlank() ||
//                !request.getDescription().isEmpty())
            requestNew.setDescription(request.getDescription());
//        if(request.getType_of_request()!=null ||
//                request.getType_of_request()!="" ||
//                !request.getType_of_request().isBlank() ||
//                !request.getType_of_request().isEmpty())
            requestNew.setType_of_request(request.getType_of_request());
//        if(request.getSubject_of_procurement()!=null ||
//                request.getSubject_of_procurement()!="" ||
//                !request.getSubject_of_procurement().isBlank() ||
//                !request.getSubject_of_procurement().isEmpty())
            requestNew.setSubject_of_procurement(request.getSubject_of_procurement());
//        if(request.getSum_of_procurement()!=null)
            requestNew.setSum_of_procurement(request.getSum_of_procurement());
//        if(request.getStatus()!=null ||
//                request.getStatus()!="" ||
//                !request.getStatus().isBlank() ||
//                !request.getStatus().isEmpty())
            requestNew.setStatus(request.getStatus());
        requestRepository.save(requestNew);
        return requestNew;
    }

    @Override
    public List<Request> findByFilter(Integer id, String name, String type, String subject, String status,
                                      Integer priceFrom, Integer priceTo, String result){
        return requestRepository.findAllByFilter(id, name, type, subject, status, priceFrom, priceTo, result);
    }

    @Override
    public List<Request> sortedById() {
        return requestRepository.findAllSortedById();
    }

}
