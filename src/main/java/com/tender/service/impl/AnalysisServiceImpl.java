package com.tender.service.impl;

import com.tender.model.Analysis;
import com.tender.model.Request;
import com.tender.repository.AnalysisRepository;
import com.tender.service.AnalysisService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AnalysisServiceImpl implements AnalysisService {
    private final AnalysisRepository analysisRepository;

    public AnalysisServiceImpl(AnalysisRepository analysisRepository) {
        this.analysisRepository = analysisRepository;
    }

    @Override
    public List<Analysis> findAll(){
        return analysisRepository.findAll();}

    @Override
    public  Analysis findAllById(Integer id){
        return  analysisRepository.findById(id).orElse(null);
    }

    @Override
    public  void deleteById(Integer id){
        analysisRepository.deleteById(id);
    }

    @Override
    public  Analysis postById(Analysis analysis){
        return  analysisRepository.save(analysis);
    }

    @Override
    public List<Analysis> findF(){
        return analysisRepository.findFirstFive();
    }

    @Override
    public Analysis findByRequestId(Integer id){return analysisRepository.findByRequestId(id);}

}
