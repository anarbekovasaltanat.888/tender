package com.tender.service.impl;

import com.tender.service.CoefficientService;
import org.springframework.stereotype.Service;

@Service
public class CoefficientServiceImpl implements CoefficientService {
    @Override
    public Double calculateCoefficient(Double sum, Integer quantity) {
        return sum/quantity;
    }
}
