package com.tender.service;


import com.tender.model.Organizer;

import java.util.List;

public interface OrganizerService  {
    List<Organizer> findAll();
    Organizer findAllById(Integer id);

    void deleteById(Integer id);
    Organizer postById(Organizer organizer);
    Organizer updateById(Integer id,Organizer organizer);
}
