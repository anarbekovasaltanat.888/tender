package com.tender.service;

import com.tender.model.Analysis;

import java.util.List;

public interface AnalysisService {
    List<Analysis> findAll();
    Analysis findAllById(Integer id);
    void deleteById(Integer id);
    Analysis postById(Analysis analysis);
    List<Analysis> findF();
    Analysis findByRequestId(Integer id);
}
