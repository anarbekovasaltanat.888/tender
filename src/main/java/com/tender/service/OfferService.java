package com.tender.service;

import com.tender.model.Offer;
import com.tender.model.dto.NameDto;

import java.util.List;

public interface OfferService {
    List<Offer> findAll();
    Offer findAllById(Integer id);

    void deleteById(Integer id);

    Offer postById(Offer offer);
    Offer updateById(Integer id,Offer offer);

//    NameDto findName(Integer id);
}
