package com.tender.service;
import com.tender.model.Request;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface RequestService {
    List<Request> findAll();
    Request findAllById(Integer id);

    void deleteById(Integer id);
    Request postById(Request request);
    Request updateById(Integer id,Request request);
    List<Request> findByFilter(Integer id, String name, String type, String subject, String status,
                               Integer priceFrom, Integer priceTo, String result);

    List<Request> sortedById();

}
