package com.tender.service;

import com.tender.model.Contractor;

import java.util.List;
import java.util.Optional;

public interface ContractorService {
    List<Contractor> findAll();

    Contractor findAllById(Integer id);

    void deleteById(Integer id);
    Contractor postById(Contractor contractor);
    Contractor updateById(Integer id,Contractor contractor);
}
