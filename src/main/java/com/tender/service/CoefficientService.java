package com.tender.service;

public interface CoefficientService {
    Double calculateCoefficient(Double sum, Integer quantity);
}
