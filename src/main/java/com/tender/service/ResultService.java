package com.tender.service;

import com.tender.model.dto.CashFlows;

import java.util.List;

public interface ResultService {
    List<CashFlows> calculateResult();
}
